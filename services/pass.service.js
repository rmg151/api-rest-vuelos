'use strict'

const bcrypt = require('bcrypt');

//Funcion que encripte el pass, devuelve un hash con salt incluido
function encriptaPassword(pass)
{
    return bcrypt.hash(pass, 10);
}

//Devuelve true o false si coincide o non
function comparaPassword(password, hash)
{
    return bcrypt.compare(password, hash);
}


module.exports = {
    encriptaPassword,
    comparaPassword
};
